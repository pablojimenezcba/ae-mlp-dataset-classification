import numpy as np
import abc
import tensorflow as tf
from scipy import ndimage

class RotationGenerator(tf.keras.utils.Sequence):
    def __init__(self,x,batch_size, max_angle = 90, shuffle=True, seed=None, order = 0):
        """ Augmentation generator for model
        Args:
            x: data to augment
            batch_size (int): Batch size for training
            max_angle (float, optional): Max angle for rotation. Defaults to 90.
            shuffle (bool, optional): If True shuffles indexes on epoch end. Defaults to True.
            seed (int, optional): Random generator seed. If None, creates random one. Defaults to None.
            order (int, optional): B-spline order for rotation. Defaults to 0.
        """
        self.x = x
        self.batch_size = batch_size
        self.max_angle = max_angle
        self.shuffle = shuffle
        self.seed = seed
        self.order = order
        self.rng = np.random.RandomState(seed)
        self.n = len(self.x)

        self.indexes = np.arange(self.n)
        np.random.shuffle(self.indexes)

    def __len__(self):
        return self.n // self.batch_size
    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indexes)
    @abc.abstractmethod
    def __getitem__(self, index):
        raise NotImplementedError('Should implement __getitem__()')
    

class Rotation_Bagging(RotationGenerator):

    def __init__(self, patchs, CMs, labels, batch_size, max_angle = 0, shuffle=True, seed=None, order = 0,return_CM=True,return_reconstruction=False):
        """ Augmentation generator for autoencoder+classifier model with unbalanced data. bootstrap method
        Args:
            patchs (2d-array): Data patches of size sqrt(2)* size of original patches
            CMs (2d-array): CM relative position. considered 3 dimensional.
            reconstruction (2d-array): Data to reconstruct. Must have same size of Patchs
            labels (1d-array): Expected output.
            batch_size (int): Batch size for training
            max_angle (float, optional): Max angle for rotation. Defaults to 90.
            shuffle (bool, optional): If True shuffles indexes on epoch end. Defaults to True.
            seed (int, optional): Random generator seed. If None, creates random one. Defaults to None.
            order (int, optional): B-spline order for rotation. Defaults to 0.
            return_reconstruction (bool, optional): If True returns reconstruction. Defaults to False.
        """
        super().__init__(patchs, batch_size, max_angle, shuffle, seed, order)

        self.cms = CMs
        self.labels = labels
        self.return_reconstruction = return_reconstruction
        self.return_CM = return_CM
        self.indexes1 = np.where(labels==0)[0]
        self.indexes2 = np.where(labels==1)[0]
        self.ratio = 0.5
        self.on_epoch_end()

    def __len__(self):
        s1 = int(self.batch_size*(1-self.ratio))
        return self.indexes1.shape[0]//s1

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indexes1)
            np.random.shuffle(self.indexes2)

    def __getitem__(self, index):
        #random bagging. repeated values
        indexes1 = self.indexes1[self.rng.choice(self.indexes1.shape[0],int(self.batch_size*self.ratio))]
        indexes2 = self.indexes2[self.rng.choice(self.indexes2.shape[0],int(self.batch_size*(1-self.ratio)))]
        
        indexes = np.concatenate((indexes1,indexes2))
        np.random.shuffle(indexes)
        
        Y=self.labels[indexes]
        P= self.x[indexes]
        Cm= self.cms[indexes] #centro de masa
        
        if self.rng.random()>=0.5:
            P = np.flip(P,axis=1)
            Cm[:,1]*=-1
        if self.rng.random()>=0.5:
            P = np.flip(P,axis=2)
            Cm[:,2]*=-1
        if self.rng.random()>=0.5:
            P = np.transpose(P,(0,2,1,3))
            if Cm.shape[1]==3:
                Cm = Cm[:,[0,2,1]]
            else:
                Cm = Cm[:,[0,2,1,3]]
        
        
        if self.rng.random()>=0.5:
            angle = self.rng.random()*self.max_angle #tiene que ser max angle
            P = ndimage.rotate(P, angle,axes=(1,2), reshape=False, order=self.order)
            rad = np.deg2rad(angle)
            rot_matrix = np.array([[np.cos(rad), -np.sin(rad)], [np.sin(rad), np.cos(rad)]])
            Cm[:,1:] = np.einsum('ij,ajk->aik',rot_matrix,Cm[:,1:])
        
        p_size = int(P.shape[1]/np.sqrt(2))
        p_size+=1-p_size%2
        inv_box = (P.shape[1]-p_size)//2
        P = P[:,inv_box:-inv_box,inv_box:-inv_box]
        if self.return_reconstruction:
            return [P,Cm], [P,Y]
        if self.return_CM:
            return [P,Cm],Y
        return P,P