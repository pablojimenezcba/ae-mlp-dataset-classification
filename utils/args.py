import argparse

# Argumentos por linea de comandos
parser = argparse.ArgumentParser()
parser.add_argument(
		"-lr_ae_indep",
		"--autoencoder indep learning_rate",
		type=float,
		default=1e-3,
		help="Learning rate (default: 1e-3)",
)


parser.add_argument(
		"-lr_ae",
		"--autoencoder learning_rate",
		type=float,
		default=1e-7,
		help="Learning rate (default: 1e-7)",
)

parser.add_argument(
		"-lr_mlp",
		"--MLP learning_rate",
		type=float,
		default=1e-5,
		help="Learning rate (default: 1e-5)",
)

parser.add_argument(
		"-lr_enc_mlp",
		"--ENC MLP learning_rate",
		type=float,
		default=1e-3,
		help="Learning rate (default: 1e-3)",
)

parser.add_argument(
		"-rf1",
		"--l1_regularizer_factor",
		type=float,
		default=0,
		help="l1 Regularizer factor (default: 0)",
)
parser.add_argument(
		"-rf2",
		"--l2_regularizer_factor",
		type=float,
		default=0,
		help="l2 Regularizer factor (default: 0)",
)
parser.add_argument(
		"-e_ae",
		"--autoencoder epochs",
		type=int,
		default=100,
		help="Epochs (default: 50)",
)
parser.add_argument(
		"-e_ae_mlp",
		"--autoencoder MLP epochs",
		type=int,
		default=2000,
		help="Epochs (default: 50)",
)
parser.add_argument(
		"-bs",
		"--batch_size",
		type=int,
		default=32,
		help="Batch size (default: None)",
)#

parser.add_argument(
		"-dp",
		"--data_path",
		type=str,
		default= "/home/pablo/job/doctorado/data/patches/data_11x11_cine_mask.hdf5",
		help="Data .h5 path",
)

parser.add_argument(
		"-sp",
		"--save_path",
		type=str,
		default= "/home/pablo/job/doctorado/results/models",
		help="Data .h5 path",
)

parser.add_argument(
		"-reg",
		"--regression",
		type=bool,
		default= False,
		help="regression architecture",
)

parser.add_argument(
		"-loo",
		"--leave_one_out",
		type=bool,
		default= False,
		help="use only one patient for validation",
)

parser.add_argument(
		"-valp",
		"--validation_patient",
		type=int,
		default= 0,
		help="only used if loo is True. Defines the validation patient to be used",
)

parser.add_argument(
		"-kfold",
		"--k_folding",
		type=int,
		default= 0,
		help="number of folds for k-fold cross validation. If 0, no k-folding",
)

parser.add_argument(
		"-kidx",
		"--k_folding_idx",
		type=int,
		default= 0,
		help="index for k-fold cross validation. Only used if k_folding is not 0",
)

kwargs = vars(parser.parse_args())

lr_ae_indep = kwargs["autoencoder indep learning_rate"]
lr_ae = kwargs["autoencoder learning_rate"]
lr_mlp = kwargs["MLP learning_rate"]
lr_enc_mlp = kwargs["ENC MLP learning_rate"]
rf1 = kwargs["l1_regularizer_factor"]
rf2 = kwargs["l2_regularizer_factor"]
epochs_ae = kwargs["autoencoder epochs"]
epochs_ae_mlp = kwargs["autoencoder MLP epochs"]
batch_size = kwargs['batch_size']
data_path = kwargs['data_path']
save_path = kwargs['save_path']
regression = kwargs['regression']
loo = kwargs['leave_one_out']
val_patient = kwargs['validation_patient']
k_folding = kwargs['k_folding']
k_folding_idx = kwargs['k_folding_idx']


description = '# lr_ae=%.1e; lr_enc_mlp=%.1e; lr_mlp=%.1e; rf1=%.1e; rf2=%.1e; epochs_ae=%d; epochs_ae_mlp=%d; batch_size=%d; leave one out=%s; regression=%s; k-folding=%d #' % (lr_ae, lr_enc_mlp, lr_mlp, rf1, rf2, epochs_ae, epochs_ae_mlp, batch_size,loo,regression,k_folding)

print("\n"+'-'*len(description))
print(description)
print('-'*len(description)+'\n')