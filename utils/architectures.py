import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.regularizers import l1_l2


def conv_block(x, filters, kernel_size, strides, padding, rf1, rf2, activation,transpose=False):
    #always includes batch normalization
    if transpose:
        x = layers.Conv2DTranspose(filters, kernel_size, strides=strides, padding=padding, kernel_regularizer=l1_l2(rf1,rf2), use_bias=False)(x)
    else:
        x = layers.Conv2D(filters, kernel_size, strides=strides, padding=padding, kernel_regularizer=l1_l2(rf1,rf2), use_bias=False)(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation(activation)(x)
    return x

def AE(patch_size,rf1=0,rf2=0):
    #encoder
    input_patch = layers.Input(shape=(patch_size,patch_size,2))
    x = input_patch
    x = conv_block(x, 256, 3, 1, "valid", rf1, rf2, 'relu')
    x = conv_block(x, 128, 3, 1, "valid", rf1, rf2, 'relu')
    x = conv_block(x, 64, 3, 2, "valid", rf1, rf2, 'relu')
    shape_before_flatten = K.int_shape(x)[1:]
    x = layers.Flatten()(x)
    x = layers.Dense(128,activation='linear') #latent features

    encoder = keras.Model(inputs=input_patch,outputs=x)
    #decoder
    input_features = layers.Input(shape=(128,))
    x = input_features
    x = layers.Reshape(shape_before_flatten)(x)
    x = conv_block(x, 64, 3, 2, "valid", rf1, rf2, 'relu',transpose=True)
    x = conv_block(x, 128, 3, 1, "valid", rf1, rf2, 'relu',transpose=True)
    x = conv_block(x, 2, 3, 1, "valid", rf1, rf2, 'linear',transpose=True)
    
    decoder = keras.Model(inputs=input_features,outputs=x)
    return encoder,decoder

def AE_classif(features_shape,drop_arg=0.5,rf1=0,rf2=0):
    input_features = layers.Input(shape=(features_shape))
    input_cm = layers.Input(shape=(3,1))
    
    x = input_features
    y = layers.Flatten()(input_cm)

    x = layers.Dense(256, 'relu', kernel_regularizer=l1_l2(rf1,rf2))(x)
    x = layers.Dropout(drop_arg)(x)
    x = layers.Dense(128, 'relu', kernel_regularizer=l1_l2(rf1,rf2))(x)
    x = layers.Dropout(drop_arg)(x)
    x = layers.concatenate([x, y])
    x = layers.Dense(1, 'sigmoid',kernel_regularizer=l1_l2(rf1,rf2))(x)
    
    classif = keras.Model(inputs=[input_features,input_cm],outputs=x)
    return classif

def AE_MLP(patch_size,drop_arg=0,rf1=0,rf2=0):
    encoder,decoder = AE(patch_size,rf1,rf2)
    classif = AE_classif(encoder.output_shape[1:],drop_arg,rf1,rf2)
    return encoder,decoder,classif