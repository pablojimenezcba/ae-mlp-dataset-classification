import tensorflow as tf
import tensorflow.keras.backend as K

class BalancedBAcc(tf.keras.metrics.Metric):

  def __init__(self,weights, name='balanced_acc', **kwargs):
    super(BalancedBAcc, self).__init__(name=name, **kwargs)
    self.one_weight,self.zero_weight = weights
    self.acc = self.add_weight(name='acc', initializer='zeros')

  def update_state(self, y_true, y_pred):
    y_true = K.cast(y_true, tf.float32)
    # Calculate the binary accuracy
    tp = K.mean(tf.multiply(y_true, y_pred))
    tn = K.mean(tf.multiply(1-y_true, 1-y_pred))
    # Apply the weights
    b_acc = tp * self.one_weight + tn * self.zero_weight
    self.acc.assign_add(b_acc)

  def result(self):
    return self.acc