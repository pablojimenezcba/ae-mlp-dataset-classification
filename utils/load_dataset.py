from dataclasses import dataclass
import numpy as np
import h5py

dt = h5py.special_dtype(vlen=str)

def get_representative_split(path,k=5,tries=1e4):
    """Returns ordered list of subjects with scar so that each fold is representative
    of the total
    """
    hf = h5py.File(path, 'r')
    sid = np.array(hf.get('ID'))
    scar = np.array(hf.get('scar-perc'))
    hf.close()
    
    mi_sid = np.where(scar > 0)[0]
    scar = scar[mi_sid]
    sid = sid[mi_sid]

    n_patchs = np.array([len(sid[sid==f]) for f in np.unique(sid)])
    n_scars = np.array([scar[sid==f].sum() for f in np.unique(sid)])
    sid = np.unique(sid)

    best = 0
    best_metric = np.inf
    
    for i in range(tries):
        if i%100==0:
            print(i,best_metric,end='\r')
        idx = np.arange(len(sid))
        np.random.shuffle(idx)
        folds = np.array_split(idx,k)
        n_patchs_folds = np.std([n_patchs[f].sum() for f in folds])/np.mean([n_patchs[f].sum() for f in folds])
        n_scars_folds = np.std([n_scars[f].sum() for f in folds])/np.mean([n_scars[f].sum() for f in folds])
        metric = [n_patchs_folds,n_scars_folds]
        if np.mean(metric)<np.mean(best_metric):
            best_metric = metric
            best = idx.copy()
    return best_metric,sid[best]


@dataclass
class DataSet:
    """Get dataset from hdf5 file and split on K folds
    """
    path: str

    def __post_init__(self):
        self._hf = None
        self.hf = h5py.File(self.path, 'r')
    
    def get_data(self):
        return self._hf
    def k_folding(self,mi_sid,k,idx):#mi subjects are taken ordered
        #mi subjects
        n_mi = len(mi_sid)
        test_idx = idx*n_mi//k
        test_sid = mi_sid[test_idx:test_idx+n_mi//k]
        train_sid = np.concatenate((mi_sid[:test_idx],mi_sid[test_idx+n_mi//k:]))
        val_sid = train_sid[-int(len(train_sid)*0.15):]
        train_sid = train_sid[:-int(len(train_sid)*0.15)]
        #control subjects
        control_sid = np.where(np.array(self.hf.get('is-normal')) == 1)[0]
        control_sid = np.unique(np.array(self.hf.get('ID'))[control_sid])
        n_control = len(control_sid)
        test_idx = idx*n_control//k
        test_sid = np.concatenate((test_sid,control_sid[test_idx:test_idx+n_control//k]))
        train_control = np.concatenate((control_sid[:test_idx],control_sid[test_idx+n_control//k:]))
        val_control = train_control[-int(len(train_control)*0.15):]
        train_control = train_control[:-int(len(train_control)*0.15)]
        train_sid = np.concatenate((train_sid,train_control))
        val_sid = np.concatenate((val_sid,val_control))
        #get data from sid
        train_data = self.get_data_from_split(train_sid)
        val_data = self.get_data_from_split(val_sid)
        test_data = self.get_data_from_split(test_sid)
        return train_data, val_data, test_data

    def get_idx_from_sid(self,sid):
        idx = np.where(np.array(self.hf.get('ID'),dtype=str) == sid)[0]
        return idx

    def get_idx_from_split(self,sids):
        idx =  self.get_idx_from_sid(sids[0])
        for sid in sids[1:]:
            patches = self.get_idx_from_sid(sid)
            idx = np.concatenate((idx,patches))
        return idx

    def get_data_from_split(self,sids):
        idx = self.get_idx_from_split(sids)
        data = {}
        for key in self.hf.keys():
            data[key] = np.array(self.hf.get(key))[idx]
        return data