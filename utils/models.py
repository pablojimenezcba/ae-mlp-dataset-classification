"""
File: models.py

Author: Pablo Javier Jimenez.

Email: pablojimenezcba@gmail.com

GitLab: https://gitlab.com/pablojimenezcba

Description: Models structure for training and saving results
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import abc
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import utils
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model
from datetime import datetime
from dataclasses import dataclass
from typing import Optional

def create_saving_folder(path):
    #create containing folder
    now = datetime.now()
    date = now.strftime("%d_%m_%Y")
    time = now.strftime("%H_%M_%S")
    folder = os.path.join(path,date)
    if not os.path.exists(folder):
        os.mkdir(folder)
    folder = os.path.join(folder,time)
    os.mkdir(folder)
    return folder

@dataclass
class Model:
    model : keras.Model
    generator : keras.utils.Sequence
    saving_path : Optional[str] = None

    @abc.abstractmethod
    def compile(self,optimizer,loss,metrics):
        raise NotImplementedError('Should implement compile()')
    def train(self,validation_data,epochs):
        raise NotImplementedError('Should implement train()')
    def save(self,args,new_folder=True):
        raise NotImplementedError('Should implement save()')
    def save_model(self,title='model'):
        path = os.path.join(self.saving_path,'models')
        if not os.path.exists(path):
            os.mkdir(path)
        #save model weights
        self.model.save(os.path.join(path,title+'.h5'))
        #save model summary
        with open(os.path.join(path,title+'_summary.txt'),'w') as f:
            self.model.summary(print_fn=lambda s: f.write(s+'\n'))
        
    def save_history(self,title='history'):
        #must have trained before
        if not hasattr(self,'history'):
            raise Exception('Model must be trained before saving history')
        path = os.path.join(self.saving_path,'history')
        if not os.path.exists(path):
            os.mkdir(path)
        np.save(
            os.path.join(path,title+'.npy'),
            self.history,
            allow_pickle=True)
        
    def plot_history(self,title='history'):
        #must have trained before
        if not hasattr(self,'history'):
            raise Exception('Model must be trained before plotting history')
        path = os.path.join(self.saving_path,'history')
        if not os.path.exists(path):
            os.mkdir(path)
        for metric in self.history.keys():
            if metric.startswith('val_'):
                continue
            fig=plt.figure()
            plt.plot(self.history[metric],color='black')
            if ('val_'+metric) in self.history.keys():
                plt.plot(self.history['val_'+metric],color='red')
                plt.legend(['training','validation'])
            else:
                plt.legend(['training'])
            plt.xlabel('Epoch',fontsize=14)
            plt.ylabel(metric,fontsize=14)
            fig.savefig(os.path.join(path,title+'_'+metric+'.pdf'),dpi=600)
            plt.close(fig)
            
    def save_args(self,args,title='args'):
        path = os.path.join(self.saving_path,'args')
        if not os.path.exists(path):
            os.mkdir(path)
        with open(os.path.join(path,title+'.txt'),'w') as f:
            for key in args.keys():
                f.write(''.join((key,'\t',str(args[key]),'\n')))
        np.save(os.path.join(path,title+'.npy'),args)
    
    def __call__(self,*args):
        return self.model(*args)
    def predict(self,*args):
        return self.model.predict(*args)

class Autoencoder(Model):
    def __init__(self,encoder,decoder,generator,saving_path=None):
        self.encoder = encoder
        self.decoder = decoder
        self.model = keras.Model(
            inputs=self.encoder.input,
            outputs=self.decoder(self.encoder.output))
        
        super(Autoencoder,self).__init__(self.model,generator,saving_path)

    def compile(self,optimizer,loss,metrics):
        self.model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def train(self, validation_data, epochs, callbacks):
        self.history = self.model.fit(
            self.generator,
            epochs=epochs,
            steps_per_epoch= len(self.generator)//self.generator.batch_size,
            validation_data=validation_data,
            callbacks=callbacks,
            verbose=2
        ).history
        return self.history
    
    def save_model(self,title='autoencoder'):
        path = os.path.join(self.saving_path,'models')
        if not os.path.exists(path):
            os.mkdir(path)
        #save model weights
        self.encoder.save(os.path.join(path,'encoder.h5'))
        self.decoder.save(os.path.join(path,'decoder.h5'))
        #save model summary
        with open(os.path.join(path,'encoder_summary.txt'),'w') as f:
            self.encoder.summary(print_fn=lambda s: f.write(s+'\n'))
        with open(os.path.join(path,'decoder_summary.txt'),'w') as f:
            self.decoder.summary(print_fn=lambda s: f.write(s+'\n'))
    
    def save(self,args,new_folder=True):
        if new_folder:
            self.saving_path = create_saving_folder(self.saving_path)
        print('Saving autoencoder in path:',self.saving_path)
        self.saving_path = os.path.join(self.saving_path,'autoencoder')
        if not os.path.exists(self.saving_path):
            os.mkdir(self.saving_path)
        self.save_model()
        super().save_history()
        super().save_args(args)

class Autoencoder_MLP(Model):
    def __init__(self,encoder,decoder,mlp,generator,saving_path=None):
        self.encoder = encoder
        self.decoder = decoder
        self.mlp = mlp
        self.model = keras.Model(
            inputs=[self.encoder.inputs,self.mlp.inputs[1]],
            outputs=mlp([self.encoder.outputs,self.mlp.inputs[1]]))
        super(Autoencoder_MLP,self).__init__(self.model,generator,saving_path)
    
    def compile(self,optimizers,losses,metrics):
        self.optimizers = optimizers
        self.losses = losses
        self.metrics = metrics
    
    @tf.function
    def train_step_enc_rec(self,x,y):
        with tf.GradientTape() as tape:
            y_pred = self.decoder(self.encoder(x, training=True), training=False)
            loss = self.losses['enc_rec'](y,y_pred)
            loss = tf.reduce_mean(loss)
            grads = tape.gradient(loss, self.encoder.trainable_variables)
            self.optimizers['enc_rec'].apply_gradients(zip(grads, self.encoder.trainable_variables))
        return loss
    
    @tf.function
    def train_step_dec_rec(self,x,y):
        with tf.GradientTape() as tape:
            y_pred = self.decoder(self.encoder(x, training=False), training=True)
            loss = self.losses['dec_rec'](y,y_pred)
            loss = tf.reduce_mean(loss)
            grads = tape.gradient(loss, self.decoder.trainable_variables)
            self.optimizers['dec_rec'].apply_gradients(zip(grads, self.decoder.trainable_variables))
        for m in self.metrics['rec']:
            m.update_state(y, y_pred)
        return loss

    @tf.function
    def train_step_classif(self,x,y):
        with tf.GradientTape() as tape:
            patch,cm = x
            y_pred = self.mlp([self.encoder(patch,training=False),cm],training=True)
            loss = self.losses['classif'](y,y_pred)
            loss = tf.reduce_mean(loss)
            grads = tape.gradient(loss, self.mlp.trainable_variables)
            self.optimizers['classif'].apply_gradients(zip(grads, self.mlp.trainable_variables))
        return loss
    
    @tf.function
    def train_step_enc_classif(self,x,y):
        with tf.GradientTape() as tape:
            patch,cm = x
            y_pred = self.mlp([self.encoder(patch,training=True),cm],training=False)
            loss = self.losses['enc_classif'](y,y_pred)
            loss = tf.reduce_mean(loss)
            grads = tape.gradient(loss, self.encoder.trainable_variables)
            self.optimizers['enc_classif'].apply_gradients(zip(grads, self.encoder.trainable_variables))            
        for m in self.metrics['classif']:
            m.update_state(y, y_pred)
        return loss
    
    @tf.function
    def train_step(self,batch):
        [x,cm], [y,yl] = batch
        # ------------ Step 1: Train MLP
        classif_loss = self.train_step_classif([x,cm],yl)
        # ------------ Step 2: Train Encoder for classification
        _ = self.train_step_enc_classif([x,cm],yl)
        # ------------ Step 3: Train Encoder for reconstruction
        _ = self.train_step_enc_rec(x,y)
        # ------------ Step 4: Train Decoder
        rec_loss = self.train_step_dec_rec(x,y)
        return classif_loss,rec_loss
    
    @tf.function
    def test_step(self,x_,y_):
        [x,cm] = x_
        [y,yl] = y_
        y_pred = self.decoder(self.encoder(x,training=False),training=False)
        rec_loss_val = self.losses['dec_rec'](y, y_pred)
        yl_pred = self.mlp([self.encoder(x,training=False),cm],training=False)
        classif_loss_val = self.losses['classif'](yl,yl_pred)
        for m in self.metrics['rec']:
            m.update_state(y, y_pred)
        for m in self.metrics['classif']:
            m.update_state(yl, yl_pred)
        return rec_loss_val,classif_loss_val
    
    def train(self, validation_data, epochs,verbose=2,checkpoint_path=None):
        self.history = {
            'rec_loss': [],
            'classif_loss': []
        }

        if validation_data is not None:
            self.history['val_rec_loss'] = []
            self.history['val_classif_loss'] = []

        for m in self.metrics['rec']:
            self.history['rec_'+m.name] = []
            self.history['val_rec_'+m.name] = []
        for m in self.metrics['classif']:
            self.history['classif_'+m.name] = []
            self.history['val_classif_'+m.name] = []
        
        if validation_data is not None:
            npatches_val = len(validation_data[0][0])
            niter_val = npatches_val//self.generator.batch_size
        
        for epoch in range(epochs):
            print('Epoch {}/{}'.format(epoch+1,epochs),flush=True)
            if validation_data is not None:
                prog_bar = tf.keras.utils.Progbar(len(self.generator),
                                                  width=15,
                                                  verbose=verbose)
            else:
                prog_bar = tf.keras.utils.Progbar(len(self.generator) - 1,
                                                  width=15,
                                                  verbose=verbose)

            for step in range(len(self.generator)):
                train_batch = self.generator[step]

                classif_loss, rec_loss = self.train_step(train_batch)
                # Update loss and metrics
                # Loss and metrics returns tensors
                log_values = [
                    ('rec_loss', rec_loss.numpy()),
                    ('classif_loss', classif_loss.numpy()),
                ]

                for m in self.metrics['rec']:
                    val = m.result().numpy()
                    log_values.append(('rec_'+m.name,val))
                for m in self.metrics['classif']:
                    val = m.result().numpy()
                    log_values.append(('classif_'+m.name, val))

                prog_bar.update(step, values=log_values)

            # Reset the metrics
            for m in self.metrics['rec']:
                m.reset_states()
            for m in self.metrics['classif']:
                m.reset_states()

            # Loss and metric reconstruction on the validation dataset
            if validation_data is not None:
                ae_loss_val = 0
                mlp_loss_val = 0
                for step_val in range(niter_val):
                    ids = np.r_[step_val * self.generator.batch_size:(step_val + 1) * self.generator.batch_size]
                    x_v,y_v = validation_data
                    x_v = [x_v[0][ids],x_v[1][ids]]
                    y_v = [y_v[0][ids],y_v[1][ids]]
                    rec_loss_val,classif_loss_val = self.test_step(x_v,y_v)
                    # Loss and metrics returns tensors
                    rec_loss_val += rec_loss_val.numpy()
                    classif_loss_val += classif_loss_val.numpy()

                rec_loss_val /= niter_val
                classif_loss_val /= niter_val
                # Progress Bar update
                log_values = [
                    ('val_rec_loss', rec_loss_val),
                    ('val_classif_loss', classif_loss_val)
                ]

                for m in self.metrics['rec']:
                    val = m.result().numpy()
                    log_values.append(('val_rec_' +m.name, val))
                for m in self.metrics['classif']:
                    val = m.result().numpy()
                    log_values.append(('val_classif_' +m.name, val))

                prog_bar.update(len(self.generator), values=log_values)
                # Reset the metrics
                for m in self.metrics['rec']:
                    m.reset_states()
                for m in self.metrics['classif']:
                    m.reset_states()

            # Save history
            logged_values = prog_bar._values

            #Checkpoint for best model
            val_acc = logged_values['val_classif_balanced_acc'][0] / logged_values['val_classif_balanced_acc'][1]
            val_kappa = logged_values['val_classif_kappa'][0] / logged_values['val_classif_kappa'][1]


            if epoch!=0 and val_acc > np.max(self.history['val_classif_balanced_acc']):
                self.encoder.save(os.path.join(checkpoint_path,'best_model_encoder_1.h5'))
                self.decoder.save(os.path.join(checkpoint_path,'best_model_decoder_1.h5'))
                self.mlp.save(os.path.join(checkpoint_path,'best_model_mlp_1.h5'))
            if epoch!=0 and val_kappa > np.max(self.history['val_classif_kappa']):
                self.encoder.save(os.path.join(checkpoint_path,'best_model_encoder_2.h5'))
                self.decoder.save(os.path.join(checkpoint_path,'best_model_decoder_2.h5'))
                self.mlp.save(os.path.join(checkpoint_path,'best_model_mlp_2.h5'))

            #change learning rate        
            if epoch>100 and epoch%10==0:
                enc_mlp_lr = self.optimizer_enc_mlp.lr.numpy()*0.1**(0.01)
                mlp_lr = self.optimizer_mlp.lr.numpy()*0.1**(0.01)
                K.set_value(self.optimizer_enc_mlp.learning_rate, enc_mlp_lr)
                K.set_value(self.optimizer_mlp.learning_rate, mlp_lr)
                print('New learning rate:', enc_mlp_lr, mlp_lr)
            for k in logged_values:
                self.history[k].append(logged_values[k][0] / logged_values[k][1])
            self.generator.on_epoch_end()
        return self.history
    

    def save_model(self):
        path = os.path.join(self.saving_path,'models')
        if not os.path.exists(path):
            os.mkdir(path)
        autoencoder = Autoencoder(self.encoder,self.decoder,None)
        autoencoder.save_model(os.path.join(path,'..'))
        self.mlp.save(os.path.join(path,'MLP.h5'))
        with open(os.path.join(path,'AE_MLP_summary.txt'),'w') as f:
            self.model.summary(print_fn=lambda s: f.write(s+'\n'))


    def save(self,args,title='AE_MLP',new_folder=True,checkpoint_path=None):
        if new_folder:
            self.saving_path = create_saving_folder(self.saving_path)
        print('Saving AE+MLP in path:',self.saving_path)
        self.saving_path = os.path.join(self.saving_path,title)
        if not os.path.exists(self.saving_path):
            os.mkdir(self.saving_path)
        self.save_model()
        super().save_history()
        super().save_args(args)
        if checkpoint_path is not None:
            self.move_checkpoint(checkpoint_path)
    
    def move_checkpoint(self,checkpoint_path):
            checkpoint_models = [
                'best_model_encoder_1.h5',
                'best_model_decoder_1.h5',
                'best_model_mlp_1.h5',
                'best_model_encoder_2.h5',
                'best_model_decoder_2.h5',
                'best_model_mlp_2.h5',
            ]
            for f in checkpoint_models:
                os.move(
                    os.path.join(checkpoint_path,f),
                    os.path.join(self.saving_path,f)
                    )