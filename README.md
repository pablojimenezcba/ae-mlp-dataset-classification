# ae-mlp-dataset-classification
This repository contains source code for Autoencoder + Multi-Layer Percepton implementation
for dataset classification.

## Dataset
Dataset used is a hdf5 file with Cine MRI patches of size 13x13

## Architecture
The architecture is composed of 3 parts:
- Encoder: Get patch and encode it to latent features
- Decoder: Reconstruct input patch from latent features
- Classifier: Get latent features and classify patch according to the presence or absence of fibrosis

## Learning
Performed learning in 4 steps for each batch:
- Train Classifier
- Train Encoder for classification (classifier weights are frozen)
- Train Encoder for reconstruction
- Train Decoder
