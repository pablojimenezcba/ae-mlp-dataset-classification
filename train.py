import os
import numpy as np
import h5py
import pandas as pd
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow import keras
from tensorflow.keras import losses, metrics, optimizers, models
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow_addons.metrics import CohenKappa

from utils.architectures import AE_MLP
from utils.augmentation import Rotation_Bagging
from utils.load_dataset import get_representative_split, DataSet
from utils.models import Autoencoder, Autoencoder_MLP
from utils.metrics import BalancedBAcc
from utils.args import *

SEED = 267
# Semilla aleatoria
if SEED == None:
    SEED = np.random.randint(0,1000)

#Diccionario con argumentos del modelo
args = {
    'split_seed':SEED,
    'lr_ae_indep':200,
    'lr_ae':lr_ae,
    'lr_mlp':lr_mlp,
    'lr_enc_mlp':lr_enc_mlp,
    'ae_epochs':epochs_ae,
    'ae_mlp_epochs':epochs_ae_mlp,
    'batch_size':batch_size,
    'mean_x1':0,'std_x1':0,
    'mean_x2':0,'std_x2':0,
    'k_folding':k_folding,
    'k_folding_idx': k_folding_idx
    }

# ------------------------- data genetarion and split ------------------------ #

# Paths con los datos
data_path = "/data/patches/13x13x1/data_13x13_unbalanced.hdf5"
#Datos y separacion entre training, validation y testing
ids =  get_representative_split(data_path,k=k_folding,tries=100000)
x = DataSet(data_path)
train,val,test = x.k_folding(ids,k=7,idx=k_folding_idx)

# --------------------------- get data dictionaries -------------------------- #
x_train = train['rot_patch'][...,None] #dimensions higher than input. Used for data augmentation
x_test = test['patch'][...,None]
x_val = val['patch'][...,None]
#concatenate segmentation as second channel
x_train = np.concatenate((x_train,train['rot-mask'][...,None]),axis=-1)
x_test = np.concatenate((x_test,test['mask'][...,None]),axis=-1)
x_val = np.concatenate((x_val,val['mask'][...,None]),axis=-1)

y_train, y_test, y_val = ( f['is-scar'][...,None] for f in  (train,test,val))
cm_train, cm_test, cm_val = ( f['d_CM'][...,None] for f in  (train,test,val))
id_train, id_test, id_val = ( np.unique(f['ID']) for f in  (train,test,val))
d_train, d_test, d_val = (f['is-normal'] for f in (train,test,val))

# --------------------------- Dataset normalization -------------------------- #

#Channel 1
mean1 = x_train[...,0].mean()
std1 = x_train[...,0].std()

x_train[...,0] = (x_train[...,0]-mean1)/std1
x_test[...,0] = (x_test[...,0]-mean1)/std1
x_val[...,0] = (x_val[...,0]-mean1)/std1

#Channel 2
mean2 = x_train[...,1].mean()
std2 = x_train[...,1].std()

x_train[...,1] = (x_train[...,1]-mean2)/std2
x_test[...,1] = (x_test[...,1]-mean2)/std2
x_val[...,1] = (x_val[...,1]-mean2)/std2

#Save normalization parameters
args['mean_x1'] = mean1
args['std_x1'] = std1
args['mean_x2'] = mean2
args['std_x2'] = std2

# ------------------------------- architecture ------------------------------- #
size = x_val.shape[-2]
encoder,decoder,classif = AE_MLP(size,drop_arg=0.5,rf1=0,rf2=0)
# ---------------------- callbacks and data augmentation --------------------- #

# learning rate reduction
def decay_schedule(epoch, lr):
    # decay by 0.1 every 5 epochs; use `% 1` to decay after each epoch
    if ((epoch % 10) == 0) and lr >= 1e-6 and epoch != 0:
        lr = lr * (0.1**0.02)
    return lr
lr_scheduler = LearningRateScheduler(decay_schedule, verbose=1)

#Rotation generator. Includes bagging
generator = Rotation_Bagging(x_train, cm_train, y_train, args['batch_size'], max_angle = 90, return_CM=False,return_reconstruction=False)

# -------------------------------- autoencoder ------------------------------- #
ae_callbacks = [lr_scheduler]
ae_loss = keras.losses.MeanAbsoluteError(name='loss')
ae_metrics = [tf.keras.metrics.MeanSquaredError(name='mse')]
ae_optimizer = keras.optimizers.Adam(learning_rate=args['lr_ae_indep'])
result_path = '../results'

ae = Autoencoder(encoder,decoder,generator,result_path)
ae.compile(optimizer=ae_optimizer,loss=ae_loss,metrics=ae_metrics)

#train
ae.train((x_val,x_val),args['ae_epochs'],ae_callbacks)
#save results
ae.save(args)
# ------------------------------ autoencoder MLP ----------------------------- #
#Get class weights for balanced metrics
weight_scar = y_train.shape[0] / (2 * (y_train.sum()))
weight_healthy = y_train.shape[0] / (2 * (1 - y_train.sum()))

classif_metrics = [
    tf.keras.metrics.BinaryAccuracy(name='acc'),
    BalancedBAcc((weight_healthy,weight_scar),name='balanced_acc'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.TruePositives(name='tp'),
    tf.keras.metrics.FalseNegatives(name='fn'),
    CohenKappa(num_classes=2,name='kappa')
]

metrics = {'classif':classif_metrics,'rec':ae_metrics}

#train reconstruction separately
optimizers = {
    'dec_rec':keras.optimizers.Adam(learning_rate=lr_ae_indep),
    'enc_rec':keras.optimizers.Adam(learning_rate=lr_ae),
    'classif':keras.optimizers.Adam(learning_rate=lr_mlp),
    'enc_classif':keras.optimizers.Adam(learning_rate=lr_enc_mlp)
}

losses = {
    'dec_rec':ae_loss,
    'enc_rec':ae_loss,
    'classif':keras.losses.BinaryCrossentropy(name='loss'),
    'enc_classif':keras.losses.BinaryCrossentropy(name='loss')
}
#save results along with AE
result_path = os.path.join(ae.saving_path,'..')
#checkpoint for best classification model
checkpoint_filepath = '.'
#adapt generator to return (x,cm),(x,y)
generator.return_reconstruction = True
generator.return_CM = True

ae_mlp = Autoencoder_MLP(encoder,decoder,classif,generator)
ae_mlp.compile(optimizers,losses,metrics)
validation_data = ([x_val,cm_val],[x_val,y_val])

ae_mlp.train(validation_data,args['ae_mlp_epochs'],checkpoint_path=checkpoint_filepath)

#save results
ae_mlp.saving_path = os.path.join(ae.saving_path,'..')
ae_mlp.save(args,new_folder=False,checkpoint_path=checkpoint_filepath)
#save split
patients = {'ID':None,'Split':None,'is-normal':None}

patients['ID'] = np.concatenate((id_train,id_val,id_test))
patients['is-normal'] = np.concatenate((d_train,d_val,d_test))
patients['Split'] = ['train' for f in id_train]+['val' for f in id_val]+['test' for f in id_test]
pd.DataFrame(patients).to_csv(os.path.join(ae.path,'..','split.csv'))